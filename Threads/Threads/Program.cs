﻿using System;
using System.Threading;

namespace Threads
{
    class Program
    {
      /*
            EX1
        
       * static void Main(string[] args)
        {
            string[] text1 = new string[] {"Una " , "vegada ", "hi ", "havia ", "un ", "gat. " };
            string[] text2 = new string[] {"En " , "un ", "lugar ", "de ", "la ", "Mancha. " };
            string[] text3 = new string[] {"Once " , "upon ", "a ", "time ", "in ", "the ", "West" };
            Thread thread1 = new Thread(() =>

            {
                foreach(string a in text1)
                {
                    Console.Write(a);
                    Thread.Sleep(1000);
                }
                Console.WriteLine();
            }
            
            );

            Thread thread2 = new Thread(() =>
            {

                thread1.Join();
                foreach (string a in text2)
                {
                    Console.Write(a);
                    Thread.Sleep(1000);
                }
                Console.WriteLine();


            });


            Thread thread3 = new Thread(() =>
            {

                thread2.Join();
                foreach (string a in text3)
                {
                    Console.Write(a);
                    Thread.Sleep(1000);
                }
                Console.WriteLine();


            });

            thread1.Start();
            thread3.Start();
            thread2.Start();
        
        }
      
    */

        //EX 2
      
        static void Main(string[] args)
        {
            int cerveses = new Random().Next(0, 9);
            Thread thread1 = new Thread(() => { omplirCervesa(ref cerveses,"Anitta");  }

           );

            Thread thread2 = new Thread(() => { thread1.Join(); treureCervesa(ref cerveses, "Anitta2"); }

           );

            Thread thread3 = new Thread(() => { thread2.Join(); treureCervesa(ref cerveses, "Anitta3");

            }

           );


            thread1.Start();
            thread2.Start();
            thread3.Start();
        }

        private static void omplirCervesa(ref int cerveses,string name)
        {
            int random = new Random().Next(0, 9);
            int check = cerveses + random;
            if (check >= 9) cerveses = 9;
            else cerveses = check;
            Console.WriteLine($"{name} ha portat un total de {cerveses}");

        }
        private static void treureCervesa(ref int cerveses,string name)
        {
            int random = new Random().Next(0, 9);
            int check = cerveses - random;
            if (check < 0) {  Console.WriteLine($"{name} ha intentat treure un total de {random} cerveses, però ha tret unt total de {cerveses}"); cerveses = 0; }
            else {
                Console.WriteLine($"{name} ha tret un total de {random} cerveses");
                cerveses = check; 
            
            }
        }
   
    }
}
